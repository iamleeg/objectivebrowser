//
//  FZAMethodDefinition.m
//  ObjectiveBrowser
//
//  Created by Graham Lee on 05/05/2012.
//  Copyright (c) 2012 Fuzzy Aliens Ltd.. All rights reserved.
//

#import "FZAMethodDefinition.h"

@implementation FZAMethodDefinition

@synthesize type;
@synthesize selector;
@synthesize sourceCode;

@end
