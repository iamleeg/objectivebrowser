//
//  FZAMethodPrintingParserDelegate.h
//  ObjectiveBrowser
//
//  Created by Graham Lee on 07/05/2012.
//  Copyright (c) 2012 Fuzzy Aliens Ltd.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FZAClassParserDelegate.h"

@interface FZAMethodPrintingParserDelegate : NSObject <FZAClassParserDelegate>

@end
